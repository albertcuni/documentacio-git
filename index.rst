Documentació git
=================
Instal·lació de git
~~~~~~~~~~~~~~~~~

Per instal·lar git a Linux:

.. code-block:: bash

    sudo apt install git

En cas de fer servir Windows:

.. code-block:: bash

   Instal·lem el git bash que haureu de buscar a internet.


Creació claus SSH
~~~~~~~~~~~~~~~~~

Si tenim una clau ssh, no ens farà falta haver d'identificar.
Per crear una clau ssh es fa de la següent manera.

.. code-block:: bash

    ssh-keygen -t ed25519 -C "email@example.com"

Una vegada tinguem la clau ssh haurem de guardar a git i no farà falta
utilitzar ni usuari ni contrassenya.

Comandes bàsiques git
~~~~~~~~~~~~~~~~~~~~~
Per a poder descarregar un projecte de git:

.. code-block:: bash

    git clone "URL del repositori"
    

Per afegir arxius a git:

.. code-block:: bash

    git add .
    
Creació del commit, que veurem a l'historial de modificacions.

.. code-block:: bash

    git commit-m "Descripció"

Per a pujar el canvi a git lab.   

.. code-block:: bash

    git push
    
Per a descarregar canvis fets:

.. code-block:: bash

    git pull

Per veure  els arxius modificats:

.. code-block:: bash

    git status

En cas de voler veure les modificacions que hem fet o que hi havien anteriorment:

.. code-block:: bash

    git diff

En cas de voler eliminar un arxiu de git:

.. code-block:: bash

    git rm "Nom arxiu"

En cas de voler saber més comandes de git podeu utilitzar la següent comanda
i sortirà un llistat de les comandes i l'explicació:

.. code-block:: bash

    git help
     
Creació de rames
~~~~~~~~~~~~~~~~

Una rama serveix per a poder fer modificacions del projecte sense haver de modificar l'original,
el que significa és que la part funcional del programa la podràs fer funcionar correctament, 
i podràs fer proves sense perillar el projecte.

.. code-block:: bash

    git git branch "Nom de la rama"
    
Per a poder llistar les rames que tenim

.. code-block:: bash

    git git branch -l
    
Per a poder moure'ns entre les rames:

.. code-block:: bash

    git checkout "Nom de la rama"
    

    
    